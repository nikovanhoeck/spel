package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import spel.Spelcontroller;

import java.io.IOException;


public class Controller {
    @FXML
    public Button play;

    public void klik(ActionEvent event){
        try {
        System.out.println("klik");
        Stage oudeScherm = (Stage) this.play.getScene().getWindow();
        Scene oudeScene = this.play.getScene();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../spel/scherm.fxml"));
        Parent root = fxmlLoader.load();
        Spelcontroller spelcontroller = fxmlLoader.getController();

        Scene scene = new Scene(root, 400, 600);
        oudeScherm.setScene(scene);
        oudeScherm.show();

        spelcontroller.initialise();

        scene.getRoot().requestFocus();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void exit(){
        System.exit(0);
    }
}
