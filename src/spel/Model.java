package spel;

import java.util.Random;

/**
 * Created by larss on 11/02/2018.
 */
public class Model {
    // declares an array of integers int[] anArray;
// }// allocates memory for 16 intergers
    private int[] lijstNummers = new int[16];
    private Random willekeurig = new Random();

    public Model() {
        /*lijstNummers[0] = 100;
        lijstNummers[1] = 200;
        lijstNummers[2] = 300;
        lijstNummers[3] = 400;
        lijstNummers[4] = 500;
        lijstNummers[5] = 600;
        lijstNummers[6] = 700;
        lijstNummers[7] = 800;
        lijstNummers[8] = 900;
        lijstNummers[9] = 1000;
        lijstNummers[10] = 1100;
        lijstNummers[11] = 1200;
        lijstNummers[12] = 1300;
        lijstNummers[13] = 1400;
        lijstNummers[14] = 1500;
        lijstNummers[15] = 1600;*/

        for (int i = 0; i < 16; i++) {
            lijstNummers[i] = 0;
        }

        int eersteBlokje = willekeurig.nextInt(16);
        int tweedeBlokje = willekeurig.nextInt(16);
        do {
            if (eersteBlokje == tweedeBlokje) {
                tweedeBlokje = willekeurig.nextInt(16);
            }
        } while (eersteBlokje == tweedeBlokje);
        lijstNummers[eersteBlokje] = 2;
        lijstNummers[tweedeBlokje] = 2;

    }

    public int[] getLijstNummers() {
        return lijstNummers;
    }

    public int getBlokje(int bloknummer) {
        return this.lijstNummers[bloknummer];
    }

    public void zetWillekeurigNummer() {
        // Hier gaan we een willekeurig nummertje plaatsen, dus 2 op een willekeurige locatie.
        // Die willekeurige locatie is hetzelfde als erboven met de eerste en tweede blokje.
        // Nu moet je dus maar 1 blokje hebben in plaats van twee.
        int eersteBlokje = willekeurig.nextInt(16);
        boolean emptySpace = false;
        for (int i = 0; i < lijstNummers.length; i++) {
            if (lijstNummers[i] == 0){
                emptySpace = true;
            }
        }
        if(emptySpace) {
            do {
                if (!(lijstNummers[eersteBlokje] == 0)){
                    eersteBlokje = willekeurig.nextInt(16);
                }
            }while (lijstNummers[eersteBlokje] >= 2);
            lijstNummers[eersteBlokje] = 2;
        }
    }
}
