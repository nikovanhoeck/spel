package spel;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class Spelcontroller {
    @FXML
    public Label score;
    @FXML
    public Label blokjeEen;
    @FXML
    public Label blokjeTwee;
    @FXML
    public Label blokjeDrie;
    @FXML
    public Label blokjeVier;
    @FXML
    public Label blokjeVijf;
    @FXML
    public Label blokjeZes;
    @FXML
    public Label blokjeZeven;
    @FXML
    public Label blokjeAcht;
    @FXML
    public Label blokjeNegen;
    @FXML
    public Label blokjeTien;
    @FXML
    public Label blokjeElf;
    @FXML
    public Label blokjeTwaalf;
    @FXML
    public Label blokjeDertien;
    @FXML
    public Label blokjeVeertien;
    @FXML
    public Label blokjeVijftien;
    @FXML
    public Label blokjeZestien;
    @FXML
    public Label[] alleBlokjes = new Label[16];

    int[] blokjes;


    public void sendpointtoscore() {
        score.setText("10");
    }

    @FXML
    public void initialise() {
        alleBlokjes[0] = blokjeEen;
        alleBlokjes[1] = blokjeTwee;
        alleBlokjes[2] = blokjeDrie;
        alleBlokjes[3] = blokjeVier;
        alleBlokjes[4] = blokjeVijf;
        alleBlokjes[5] = blokjeZes;
        alleBlokjes[6] = blokjeZeven;
        alleBlokjes[7] = blokjeAcht;
        alleBlokjes[8] = blokjeNegen;
        alleBlokjes[9] = blokjeTien;
        alleBlokjes[10] = blokjeElf;
        alleBlokjes[11] = blokjeTwaalf;
        alleBlokjes[12] = blokjeDertien;
        alleBlokjes[13] = blokjeVeertien;
        alleBlokjes[14] = blokjeVijftien;
        alleBlokjes[15] = blokjeZestien;

        blokjes = spel.getLijstNummers();


        for (int i = 0; i < 16; i++) {
            if (blokjes[i] == 0) {
                alleBlokjes[i].setText("");
            } else {
                alleBlokjes[i].setText(String.valueOf(blokjes[i]));
            }
        }

    }

    private Model spel = new Model();

    public void toonallecijfers() {
        for (int i = 0; i < 16; i++) {
            if (blokjes[i] == 0) {
                alleBlokjes[i].setText("");
            } else {
                alleBlokjes[i].setText(String.valueOf(blokjes[i]));
            }
        }
    }

    public void pijltjes(KeyEvent event) {
        System.out.println("Arrow");
        boolean zitNietVast = false;
        int loop = 0;
        boolean blokjesZijnBewogen = false;
        if (event.getCode().equals(KeyCode.LEFT)) {
            System.out.println("Links");
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarlinks();
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
            do {
                blokjesZijnBewogen = false;
                if (blokjes[14] == blokjes[15] & blokjes[15] > 0) {
                    blokjes[14] = blokjes[15] * 2;
                    blokjes[15] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[14]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[13] == blokjes[14] & blokjes[14] > 0) {
                    blokjes[13] = blokjes[14] * 2;
                    blokjes[14] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[13]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[12] == blokjes[13] & blokjes[13] > 0) {
                    blokjes[12] = blokjes[13] * 2;
                    blokjes[13] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[12]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[10] == blokjes[11] & blokjes[11] > 0) {
                    blokjes[10] = blokjes[11] * 2;
                    blokjes[11] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[10]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[9] == blokjes[10] & blokjes[10] > 0) {
                    blokjes[9] = blokjes[10] * 2;
                    blokjes[10] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[9]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[8] == blokjes[9] & blokjes[9] > 0) {
                    blokjes[8] = blokjes[8] * 2;
                    blokjes[9] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[8]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[6] == blokjes[7] & blokjes[7] > 0) {
                    blokjes[6] = blokjes[7] * 2;
                    blokjes[7] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[6]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[5] == blokjes[6] & blokjes[6] > 0) {
                    blokjes[5] = blokjes[6] * 2;
                    blokjes[6] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[5]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[4] == blokjes[5] & blokjes[5] > 0) {
                    blokjes[4] = blokjes[5] * 2;
                    blokjes[5] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[4]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[3] == blokjes[2] & blokjes[2] > 0) {
                    blokjes[2] = blokjes[2] * 2;
                    blokjes[3] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[2]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[2] == blokjes[1] & blokjes[1] > 0) {
                    blokjes[1] = blokjes[1] * 2;
                    blokjes[2] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[1]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[1] == blokjes[0] & blokjes[0] > 0) {
                    blokjes[0] = blokjes[0] * 2;
                    blokjes[1] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[0]));
                    blokjesZijnBewogen = true;
                }
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
        }
        blokjesZijnBewogen = false;
        if (event.getCode().equals(KeyCode.RIGHT)) {
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarRechts();
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;

            System.out.println("Rechts");
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarRechts();
                if (blokjes[14] == blokjes[15] & blokjes[15] > 0) {
                    blokjes[15] = blokjes[14] * 2;
                    blokjes[14] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[15]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[13] == blokjes[14] & blokjes[14] > 0) {
                    blokjes[14] = blokjes[13] * 2;
                    blokjes[13] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[14]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[12] == blokjes[13] & blokjes[13] > 0) {
                    blokjes[13] = blokjes[12] * 2;
                    blokjes[12] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[13]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[10] == blokjes[11] & blokjes[11] > 0) {
                    blokjes[11] = blokjes[10] * 2;
                    blokjes[10] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[11]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[9] == blokjes[10] & blokjes[10] > 0) {
                    blokjes[10] = blokjes[9] * 2;
                    blokjes[9] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[10]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[8] == blokjes[9] & blokjes[9] > 0) {
                    blokjes[9] = blokjes[8] * 2;
                    blokjes[8] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[9]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[6] == blokjes[7] & blokjes[7] > 0) {
                    blokjes[7] = blokjes[6] * 2;
                    blokjes[6] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[7]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[5] == blokjes[6] & blokjes[6] > 0) {
                    blokjes[6] = blokjes[6] * 2;
                    blokjes[5] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[6]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[4] == blokjes[5] & blokjes[5] > 0) {
                    blokjes[5] = blokjes[4] * 2;
                    blokjes[4] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[5]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[2] == blokjes[3] & blokjes[3] > 0) {
                    blokjes[3] = blokjes[3] * 2;
                    blokjes[2] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[3]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[1] == blokjes[2] & blokjes[2] > 0) {
                    blokjes[2] = blokjes[1] * 2;
                    blokjes[1] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[2]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[0] == blokjes[1] & blokjes[1] > 0) {
                    blokjes[1] = blokjes[0] * 2;
                    blokjes[0] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[1]));
                    blokjesZijnBewogen = true;
                }
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
        }
        blokjesZijnBewogen = false;
        if (event.getCode().equals(KeyCode.DOWN)) {
            System.out.println("beneden");
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarBeneden();
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarBeneden();
                if (blokjes[11] == blokjes[15] & blokjes[15] > 0) {
                    blokjes[15] = blokjes[11] * 2;
                    blokjes[11] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[15]));
                    blokjesZijnBewogen = true;
                    System.out.println(11);
                }
                if (blokjes[7] == blokjes[11] & blokjes[11] > 0) {
                    blokjes[11] = blokjes[7] * 2;
                    blokjes[7] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[11]));
                    blokjesZijnBewogen = true;
                    System.out.println(7);
                }
                if (blokjes[3] == blokjes[7] & blokjes[7] > 0) {
                    blokjes[7] = blokjes[3] * 2;
                    blokjes[3] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[7]));
                    blokjesZijnBewogen = true;
                    System.out.println(3);
                }
                if (blokjes[10] == blokjes[14] & blokjes[14] > 0) {
                    blokjes[14] = blokjes[10] * 2;
                    blokjes[10] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[14]));
                    blokjesZijnBewogen = true;
                    System.out.println(10);
                }
                if (blokjes[6] == blokjes[10] & blokjes[10] > 0) {
                    blokjes[10] = blokjes[6] * 2;
                    blokjes[6] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[10]));
                    blokjesZijnBewogen = true;
                    System.out.println(6);
                }
                if (blokjes[2] == blokjes[6] & blokjes[6] > 0) {
                    blokjes[6] = blokjes[2] * 2;
                    blokjes[2] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[6]));
                    blokjesZijnBewogen = true;
                    System.out.println(2);
                }
                if (blokjes[9] == blokjes[13] & blokjes[13] > 0) {
                    blokjes[13] = blokjes[9] * 2;
                    blokjes[9] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[13]));
                    blokjesZijnBewogen = true;
                    System.out.println(9);
                }
                if (blokjes[5] == blokjes[9] & blokjes[9] > 0) {
                    blokjes[9] = blokjes[5] * 2;
                    blokjes[5] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[9]));
                    blokjesZijnBewogen = true;
                    System.out.println(5);
                }
                if (blokjes[1] == blokjes[5] & blokjes[5] > 0) {
                    blokjes[5] = blokjes[1] * 2;
                    blokjes[1] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[5]));
                    blokjesZijnBewogen = true;
                    System.out.println(1);
                }
                if (blokjes[8] == blokjes[12] & blokjes[12] > 0) {
                    blokjes[12] = blokjes[8] * 2;
                    blokjes[8] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[12]));
                    blokjesZijnBewogen = true;
                    System.out.println(8);
                }
                if (blokjes[4] == blokjes[8] & blokjes[8] > 0) {
                    blokjes[8] = blokjes[4] * 2;
                    blokjes[4] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[8]));
                    blokjesZijnBewogen = true;
                    System.out.println(4);
                }
                if (blokjes[0] == blokjes[4] & blokjes[0] > 0) {
                    blokjes[4] = blokjes[0] * 2;
                    blokjes[0] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[4]));
                    blokjesZijnBewogen = true;
                    System.out.println(0);
                }
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
        }
        if (event.getCode().equals(KeyCode.UP)) {
            System.out.println("boveb");
            blokjesZijnBewogen = false;
            do {
                blokjesZijnBewogen = false;
                blokjesZijnBewogen = schuifAllesNaarBoven();
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
            do {
                if (blokjes[4] == blokjes[0] & blokjes[0] > 0) {
                    blokjes[0] = blokjes[4] * 2;
                    blokjes[4] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[0]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[8] == blokjes[4] & blokjes[4] > 0) {
                    blokjes[4] = blokjes[8] * 2;
                    blokjes[8] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[4]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[12] == blokjes[8] & blokjes[8] > 0) {
                    blokjes[8] = blokjes[12] * 2;
                    blokjes[12] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[8]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[5] == blokjes[1] & blokjes[1] > 0) {
                    blokjes[1] = blokjes[5] * 2;
                    blokjes[5] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[1]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[9] == blokjes[5] & blokjes[5] > 0) {
                    blokjes[5] = blokjes[9] * 2;
                    blokjes[9] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[5]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[13] == blokjes[9] & blokjes[9] > 0) {
                    blokjes[9] = blokjes[13] * 2;
                    blokjes[13] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[9]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[6] == blokjes[2] & blokjes[6] > 0) {
                    blokjes[2] = blokjes[6] * 2;
                    blokjes[6] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[2]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[10] == blokjes[6] & blokjes[2] > 0) {
                    blokjes[6] = blokjes[10] * 2;
                    blokjes[10] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[6]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[14] == blokjes[10] & blokjes[10] > 0) {
                    blokjes[10] = blokjes[14] * 2;
                    blokjes[14] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[10]));
                    blokjesZijnBewogen = true;
                }
                if (blokjes[7] == blokjes[3] & blokjes[3] > 0) {
                    blokjes[3] = blokjes[7] * 2;
                    blokjes[7] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[3]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[11] == blokjes[7] & blokjes[7] > 0) {
                    blokjes[7] = blokjes[11] * 2;
                    blokjes[11] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[7]));
                    blokjesZijnBewogen = true;
                }

                if (blokjes[15] == blokjes[11] & blokjes[11] > 0) {
                    blokjes[11] = blokjes[15] * 2;
                    blokjes[15] = 0;
                    score.setText(String.valueOf(Integer.parseInt(score.getText()) + blokjes[11]));
                    blokjesZijnBewogen = true;
                }
                loop++;
            } while (blokjesZijnBewogen);
            if (loop > 1) {
                zitNietVast = true;
            }
            loop = 0;
        }
        if (zitNietVast) {
            spel.zetWillekeurigNummer();
        }
        toonallecijfers();

        /*for (int i = 0; i < 16; i++) {
            System.out.println(i + " : " + spel.getBlokje(i));
        }*/


    }

    private boolean kijkVrijeBaanLinks() {
        boolean vrijeBaan = false;
        for (int i = 0; i < 16; i++) {
            if (blokjes[i] > 0 && i != 0 && i != 4 && i != 8 && i != 12) {
                if (blokjes[i - 1] == 0 || blokjes[i - 1] == blokjes[i]) {
                    vrijeBaan = true;
                }
            }
        }
        return vrijeBaan;
    }

    private boolean kijkVrijeBaanRechts() {
        boolean vrijeBaan = false;
        for (int i = 15; i >= 0; i--) {
            if (blokjes[i] > 0 && i != 15 && i != 11 && i != 7 && i != 3) {
                if (blokjes[i + 1] == 0 || blokjes[i + 1] == blokjes[i]) {
                    vrijeBaan = true;
                }
            }
        }
        return vrijeBaan;
    }

    private boolean kijkVrijeBaanBeneden() {
        boolean vrijeBaan = false;
        for (int i = 0; i < 16; i++) {
            if (blokjes[i] > 0 && i != 15 && i != 14 && i != 13 && i != 12) {
                if (blokjes[i + 4] == 0 || blokjes[i + 4] == blokjes[i + 4]) {
                    vrijeBaan = true;
                    System.out.println("Vrije baan");
                }
            }
        }
        return vrijeBaan;
    }

    private boolean kijkVrijeBaanBoven() {
        boolean vrijeBaan = false;
        for (int i = 15; i >= 0; i--) {
            if (blokjes[i] > 0 && i != 0 && i != 1 && i != 2 && i != 3) {
                if (blokjes[i - 4] == 0 || blokjes[i - 4] == blokjes[i]) {
                    vrijeBaan = true;
                }
            }
        }
        return vrijeBaan;
    }

    private boolean schuifAllesNaarRechts() {
        boolean verschoven = false;
        for (int i = 0; i < 16; i++) {
            if (i == 3 || i == 7 || i == 11 || i == 15) {
            } else {
                if (blokjes[i + 1] == 0 && blokjes[i] > 0) {
                    blokjes[i + 1] = blokjes[i];
                    blokjes[i] = 0;
                    verschoven = true;
                    System.out.println("Shuffle right");
                }

            }
        }
        return verschoven;
    }

    private boolean schuifAllesNaarlinks() {
        boolean verschoven = false;
        for (int i = 15; i >= 0; i--) {
            if (i == 0 || i == 4 || i == 8 || i == 12) {

            } else {
                if (blokjes[i - 1] == 0 && blokjes[i] > 0) {
                    blokjes[i - 1] = blokjes[i];
                    blokjes[i] = 0;
                    verschoven = true;
                }
            }
        }
        return verschoven;
    }

    public void speelOpnieuw() {
        spel = new Model();
        initialise();
    }

    private boolean schuifAllesNaarBeneden() {
        boolean verschoven = false;
        for (int i = 0; i < 16; i++) {
            if (i == 12 || i == 13 || i == 14 || i == 15) {

            } else {
                if ((i + 4 < 16)) {
                    if (blokjes[i + 4] == 0 && blokjes[i] > 0) {
                        blokjes[i + 4] = blokjes[i];
                        blokjes[i] = 0;
                        verschoven = true;
                    }
                }
            }
        }
        return verschoven;
    }

    private boolean schuifAllesNaarBoven() {
        boolean verschoven = false;
        for (int i = 15; i >= 0; i--) {
            if (i == 0 || i == 1 || i == 2 || i == 3) {

            } else {
                if ((i - 4 >= 0)) {
                    if (blokjes[i - 4] == 0 && blokjes[i] > 0) {
                        blokjes[i - 4] = blokjes[i];
                        blokjes[i] = 0;
                        verschoven = true;
                    }
                }
            }
        }
        return verschoven;
    }
}







